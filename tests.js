test("Valid Plate", function (assert) {
    assert.equal(isValidPlate("0000BBB"), true, "SAMPLE PLATE RIGHT");
    assert.equal(isValidPlate("BBBB000"), false, "SAMPLE PLATE WRONG");
});

test("Length distinct 4 for numbers", function (assert) {
    assert.equal(isValidPlate("1234BCD"), true, "SAMPLE PLATE RIGHT");
    assert.equal(isValidPlate("123BCD"), false, "LESS NUMBERS");
});

test("Length distinct 3 for letters", function (assert) {
   assert.equal(isValidPlate("4321ZYX"), true, "SAMPLE PLATE RIGHT");
   assert.equal(isValidPlate("4321ZY"), false, "LESS LETTERS"); 
});

test("Numbers are letters", function (assert) {
   assert.equal(isValidPlate("6789ZZZ"), true, "SAMPLE PLATE RIGHT");
   assert.equal(isValidPlate("ZZZZZZZ"), false, "NUMBERS ARE LETTERS");
});

test("Letters are numbers", function (assert) {
    assert.equal(isValidPlate("6789ZZZ"), true, "SAMPLE PLATE RIGHT");
    assert.equal(isValidPlate("6789999"), false, "LETTERS ARE NUMBERS");
});

test("Letters are not valid letters", function (assert) {
    assert.equal(isValidPlate("1111CCC"), true, "SAMPLE PLATE RIGHT");
    assert.equal(isValidPlate("1111ABC"), false, "LETTERS ARE VOWELS");
    assert.equal(isValidPlate("1111MNÑ"), false, "LETTERS ARE Ñ");
    assert.equal(isValidPlate("1111QRS"), false, "LETTERS ARE Q");
    assert.equal(isValidPlate("1111&RS"), false, "LETTERS ARE SPECIAL CHARACTER");
});

test("Numbers are not valid numbers", function (assert) {
   assert.equal(isValidPlate("1111CCC"), true, "SAMPLE PLATE RIGHT");
   assert.equal(isValidPlate("1-611CCC"), false, "NEGATIVE NUMBER");
});
