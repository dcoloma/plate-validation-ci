function isValidPlate(plate) { //eslint-disable-line no-unused-vars
  var re = /(\d\d\d\d[BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ])/i;
  var boolean;
  if(plate.match(re) != null) boolean=true;
  else boolean = false;

  gtag('event', 'click', { //eslint-disable-line
    'event_category': 'validate',
    'event_label': boolean
  });

  return boolean;
}